package seminar1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleTaskGeneratorTest {
    private Task task = new Task(1,2,3,4,5,6,7,8);
    private SimpleBuffer buffer = new SimpleBuffer();
    private SimpleTaskGenerator gen = new SimpleTaskGenerator(1, 8, buffer);

    @Test
    void withStartValue() {
        assertEquals(gen = new SimpleTaskGenerator(3,8,buffer),gen.withStartValue(3));
    }
    @Test
    void withAmount() {
        assertEquals(gen = new SimpleTaskGenerator(1,3,buffer),gen.withAmount(3));
    }


    @Test
    void generate() {
        gen.generate();
        Task t = (Task) buffer.getElem();
        assertArrayEquals(task.getArray(),t.getArray());
    }
}