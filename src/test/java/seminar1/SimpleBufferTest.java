package seminar1;

import org.junit.jupiter.api.Test;
import java.util.ArrayDeque;
import java.util.Locale;
import static org.junit.jupiter.api.Assertions.*;


public class SimpleBufferTest {
    @Test
    void addElem() {
        SimpleBuffer <ITask> buffer = new SimpleBuffer(1,2,3,4,5,6,7,8);
        SimpleBuffer <ITask> expected = new SimpleBuffer(1,2,3,4,5,6,7,8,2);
        buffer.addElem(2);
        assertEquals(expected, buffer);
    }

    @Test
    void getElem() {
        SimpleBuffer <ITask>buffer = new SimpleBuffer(1,2,3,4,5,6,7,8);
        assertEquals(1,buffer.getElem());
    }

    @Test
    void quantity() {
        SimpleBuffer <ITask>buffer = new SimpleBuffer(1,2,3,4,5,6,7,8);
        assertEquals(8, buffer.quantity());
    }

    @Test
    void cleanOnEmptyList() {
        SimpleBuffer <ITask> buffer2 = new SimpleBuffer();
        buffer2.checkEmpty();
    }
    @Test
    void cleanOnNonEmptyList() {
        SimpleBuffer <ITask>buffer = new SimpleBuffer(1,2,3,4,5,6,7,8);
        buffer.checkEmpty();
    }

    @Test
    void checkEmptyOnEmptyList() {
        SimpleBuffer <ITask> buffer2 = new SimpleBuffer();
        buffer2.clean();
    }
    @Test
    void checkEmptyOnNonEmptyList() {
        SimpleBuffer <ITask>buffer = new SimpleBuffer(1,2,3,4,5,6,7,8);
        buffer.clean();
    }
}