package seminar1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleTaskProcessorTest {
    private SimpleBuffer buffer = new SimpleBuffer();
    private Task task = new Task(1,2,3,4,5);
    private SimpleBuffer buffer1 = new SimpleBuffer(task);
    @Test
    void processEmptyBuffer() {
        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);
        assertEquals(null, processor.process());
    }

    @Test
    void processNotEmptyBuffer() {
        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer1);
        assertEquals(new Integer(15), processor.process());
    }
}