package seminar1;

public interface ITaskProcessor {
    Integer process();
}
