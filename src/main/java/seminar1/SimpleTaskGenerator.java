package seminar1;

public class SimpleTaskGenerator implements ITaskGenerator {
    private int startValue;
    private int amount;
    private SimpleBuffer<ITask> buffer;

    public int getStartValue() {
        return startValue;
    }

    public int getAmount() {
        return amount;
    }

    public SimpleBuffer<ITask> getBuffer() {
        return buffer;
    }

    SimpleTaskGenerator(int startValue, int amount, SimpleBuffer buffer) {
            this.buffer = buffer;
            this.amount = amount;
            this.startValue = startValue;
    }

    SimpleTaskGenerator withStartValue(int startValue) {
        this.startValue = startValue;
        return this;
    }

    SimpleTaskGenerator withAmount(int amount) {
        this.amount = amount;
        return this;
    }


    @Override
    public void generate() {
        int [] arr = new int[amount];
        for(int i = 0; i < arr.length; i++){
            arr[i] = startValue + i;
        }
        Task task = new Task(arr);
        buffer.addElem(task);
    }
}
