package seminar1;

public class SimpleTaskProcessor implements ITaskProcessor {
    private SimpleBuffer<ITask> buffer;


    SimpleTaskProcessor(SimpleBuffer buffer){
        this.buffer = buffer;
    }

   @Override
    public Integer process() {
        int sum = 0;
        if (buffer.checkEmpty()) {
            return null;
        } else {
            Task task = (Task) buffer.getElem();
            for (int i : task.getArray()) {
                sum += i;
            }
        }
        return sum;
    }
}
