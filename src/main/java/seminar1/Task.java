package seminar1;

import java.util.Arrays;

public class Task implements ITask {
    private int[] array;

    Task(int... a) {
        this.array = new int[a.length];
        for (int i = 0; i < a.length; i++){
            array[i] = a[i];
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Arrays.equals(array, task.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }

    public int[] getArray() {
        return array;
    }


    @Override
    public int[] getData() {
        return array;
    }
}
