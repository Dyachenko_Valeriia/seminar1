package seminar1;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Objects;

public class SimpleBuffer<T> implements IBuffer  {
    private ArrayDeque buffer;

    SimpleBuffer(T...arr){
        buffer = new ArrayDeque<T>();
        for(T i:arr){
            buffer.add(i);
        }

    }
    @Override
    public void addElem(Object elem) {
        buffer.addLast(elem);
    }

    @Override
    public T getElem() {
        return (T) buffer.removeFirst();
    }

    @Override
    public int quantity() {
        return buffer.size();
    }

    @Override
    public void clean() {
        buffer.clear();
    }

    @Override
    public boolean checkEmpty() {
        if (buffer.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleBuffer<?> that = (SimpleBuffer<?>) o;
        return Arrays.equals(buffer.toArray(), that.buffer.toArray());
    }

    @Override
    public int hashCode() {
        return Objects.hash(buffer);
    }
}
