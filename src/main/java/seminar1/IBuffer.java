package seminar1;


public interface IBuffer<T> {
    void addElem(T elem);
    T getElem();
    int quantity();
    void clean();
    boolean checkEmpty();
}
